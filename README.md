# HoneyPot Passwords
Author: SarahNyxx

## Logging password attempts for fun and profit
A repository for the SSH honeypot I'm running to compile a wordlist based on the automated login attempts against my network. 

If they might as well try, I might as well get something out of it, right? 

Also a fun experimentation with exploring the dataset to glean insights. Should evolve as I implement new ideas. 

## Structure

Original log files go in the `captures` directory

Parsed data goes in the `data` directory

Scripts used for parsing in the `scripts` directory

## Statistics:

Total Attempts:    150,711 (+36,048)

Password:      151,259 (+36,662)

SSH-Key:       12,079  (+3,209)

Unique Addresses:  5,282  (+1,546)

Unique Usernames:  6,088  (+860)

Unique Passwords:  26,971 (+2,406)

