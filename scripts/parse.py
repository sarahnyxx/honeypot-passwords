#!/usr/bin/python3

## Author: SarahNyxx
## https://gitlab.com/sarahnyxx/honeypot-passwords

import sys
import argparse
import textwrap

parser = argparse.ArgumentParser(\
        formatter_class=argparse.RawDescriptionHelpFormatter,\
        description=textwrap.dedent('''\
        A utility for parsing the logs from the SSH Honeypot

        Author: SarahNyxx
        https://gitlab.com/sarahnyxx/honeypot-passwords
        '''))
parser.add_argument("-i", "--input",\
        help="Input File(s)/directory for the parser",\
        metavar="FILE",\
        type=str,\
        required=True)
parser.add_argument("-a", "--address",\
        help="Output addresses from the --input file to --output file",\
        metavar="FILE",\
        type=str)
parser.add_argument("-n", "--username",\
        help="Output usernames from the --input file to --output file",\
        metavar="FILE",\
        type=str)
parser.add_argument("-p", "--password",\
        help="Output passwords from the --input file to --output file",\
        metavar="FILE",\
        type=str)
parser.add_argument("-u", "--unique",\
        help="Eliminate duplicates from output, only display unique values",\
        action="store_true")
parser.add_argument("-s", "--statistics",\
        help="Output statistics about --input file to console",\
        action="store_true")
parser.add_argument("-v", "--verbose",\
        help="Output unhandled lines to console for debugging",\
        action="store_true")
args = parser.parse_args()

## Setting up arrays for appending values
addresses=[]
usernames=[]
passwords=[]
sessions=0
exchanges=0
unhandled=0

## Getting file into an array
file = open(args.input)
lines = file.readlines()

## ooooooh boy could this use a reformat later.... for now it's working :P 

for x in lines:
    if "Session" in x:
        # Not handling logging of the sessions yet
        # Probably a more accurate count of connection attempts. 
        sessions += 1

    elif "Error exchanging keys:" in x:
        # Increment an INT since the only verbosity is "Error exchanging keys" 
        exchanges += 1
        # Log address of the bot
        addresses.append(x[5])

    else:
        # Annoying newline characters
        x = x.strip()
        # Get space-deliminated elements of line as array. 
        y = x.split()
        
        if len(y) == 8:
            # get address, username, password
            addresses.append(y[5])
            usernames.append(y[6])
            passwords.append(y[7])
        else:
            if args.verbose == True:
                print("UNHANDLED: ", x)
                unhandled += 1
## End main loop, begin processing

addresses_total = len(addresses)
addresses_unique = set(addresses)

usernames_total = len(usernames)
usernames_unique = set(usernames)
usernames_unique = sorted(usernames_unique)

passwords_total = len(passwords)
passwords_unique = set(passwords)
passwords_unique = sorted(passwords_unique)

file.close()

if args.address:
    file = open(args.address, "w")
    if args.unique:
        for x in addresses_unique:
            file.write(x+"\n")
    else:
        for x in addresses:
            file.write(x+"\n")
    file.close()

if args.username:
    file = open(args.username, "w")
    if args.unique:
        for x in usernames_unique:
            file.write(x+"\n")
    else:
        for x in usernames:
            file.write(x+"\n")
    file.close()

if args.password:
    file = open(args.password, "w")
    if args.unique:
        for x in passwords_unique:
            file.write(x+"\n")
    else:
        for x in passwords:
            file.write(x+"\n")
    file.close()

if args.statistics:
    print("Statistics:")
    print("")
    print("Total Attempts:   ", sessions)
    print("    Password:     ", passwords_total)
    print("    SSH-Key:      ", exchanges)
    print("")
    print("Unique Addresses: ", len(addresses_unique))
    print("Unique Usernames: ", len(usernames_unique))
    print("Unique Passwords: ", len(passwords_unique))
    print("")
